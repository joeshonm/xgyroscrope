/********* XGyroscope.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>


@interface XGyroscope : CDVPlugin {
  // Member variables go here.
    
    __strong CMMotionManager *mManager;
    NSMutableDictionary *gyroResult;
}

- (void)start:(CDVInvokedUrlCommand*)command;
- (void)stop:(CDVInvokedUrlCommand*)command;
@end

@implementation XGyroscope

- (void)start:(CDVInvokedUrlCommand*)command
{
    __block CDVPluginResult* pluginResult = nil;


    // Create a CMMotionManager
    mManager = [[CMMotionManager alloc] init];
    mManager.gyroUpdateInterval = 1.0f / 2.0f;
    

    // Check whether the gyroscope is available
    if (mManager.isGyroAvailable == TRUE) {
        
        [mManager startGyroUpdates];
        
        [mManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMGyroData *gyroData, NSError *error) {
            
            NSLog(@"GyroX = %f", gyroData.rotationRate.x);

            gyroResult = [[NSMutableDictionary alloc] init];
            NSNumber *rX = [[NSNumber alloc] initWithDouble:gyroData.rotationRate.x];
            [gyroResult setObject:rX forKey:@"x"];
            NSNumber *rY = [[NSNumber alloc] initWithDouble:gyroData.rotationRate.y];
            [gyroResult setObject:rY forKey:@"y"];
            NSNumber *rZ = [[NSNumber alloc] initWithDouble:gyroData.rotationRate.z];
            [gyroResult setObject:rZ forKey:@"z"];
        }];
        
        //NSLog(@"GyroX = %f", [mManager gyroData].rotationRate.x);
        //NSLog(@"GyroResult = %@", gyroResult);
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:gyroResult];
        
        
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)stop:(CDVInvokedUrlCommand*)command {
    
    __block CDVPluginResult* pluginResult = nil;
    
    @try {
        [mManager stopGyroUpdates];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"Gyro Stopped"];
    }
    
    @catch ( NSException *e ) {
       pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Gyro Stop Error"];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

@end
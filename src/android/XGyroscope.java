package cordova.plugin.xgyroscope;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.*;
import java.util.List;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Context;
import android.util.Log;

import android.os.Handler;
import android.os.Looper;

/**
 * This class echoes a string called from JavaScript.
 */

public class XGyroscope extends CordovaPlugin implements SensorEventListener {

	//the Sensor Manager
	private static Context context;
	private SensorManager sensorManager;  // Sensor manager
    private Sensor mSensor;  // AngularSpeed sensor returned by sensor manager
	private int status;
	private int accuracy = SensorManager.SENSOR_STATUS_UNRELIABLE;

	public float x = 0;
	public float y = 0;
	public float z = 0;


	/**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The associated CordovaWebView.
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        this.sensorManager = (SensorManager) cordova.getActivity().getSystemService(Context.SENSOR_SERVICE);
		this.mSensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
    }

	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

		if (action.equals("start")) {
			BigDecimal interval = new BigDecimal(args.getString(0));
			this.start(interval.floatValue(), callbackContext);
			Log.d("XGyroscope", "Start");
			return true;
        }
        else if (action.equals("stop")) {
			callbackContext.success("Stopped");
			return true;
        } else {
          	// Unsupported action
		  	callbackContext.error("Please call an existing function.");
        	return false;
        }
    }

    private void start(Float interval, CallbackContext callbackContext) {

		this.sensorManager.registerListener(this, this.mSensor, SensorManager.SENSOR_DELAY_UI);

		JSONObject obj = new JSONObject();
		
		try {
			obj.put("x", this.x);
			obj.put("y", this.y);
			obj.put("z", this.z);
			obj.put("timestamp", System.currentTimeMillis());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		this.status = 1;

		callbackContext.success(obj);

    }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy)
	{
		// Only look at gyroscope events
        if (sensor.getType() != Sensor.TYPE_GYROSCOPE) {
            return;
        }

        // If not running, then just return
        if (this.status == 0) {
            return;
        }
        this.accuracy = accuracy;
	}

	@Override
	public void onSensorChanged(SensorEvent event)
	{
		Log.d("XGyroscope", "Sensor changed");

		//if sensor is unreliable, return void
		if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE)
		{
			Log.d("XGyroscope", "Sensor unreliable");
			return;
		}

		if (event.accuracy == SensorManager.SENSOR_STATUS_NO_CONTACT) {
			Log.d("XGyroscope", "Sensor no contact");
		}
			

		if (event.accuracy >= SensorManager.SENSOR_STATUS_ACCURACY_LOW) {
			Log.d("XGyroscope", "Get Values");
            // Save time that event was received
            this.x = event.values[0];
            this.y = event.values[1];
            this.z = event.values[2];

        }

	}
}

var exec = require('cordova/exec');
var interval = null;

module.exports = {

    start: function(time, onSuccess, onError) {

        if (typeof time !== "number" || time <= 0 ) {
            return onError("Please set the time in seconds as the first parameter for XGyroscope.start()");
        }
        
        interval = setInterval(function(){
            exec(function(result){
                console.log("Gyro Start");
                return onSuccess(result);
            },
            function(error){
                console.log("Gyro Start Error");
                return onError(error);
            },"XGyroscope","start",[time]);
        }, (time * 1000));
        
    },

    stop: function() {
        if (interval != null) {
            clearInterval(interval);

            exec(function(result){
                console.log("Gyro Stop");
            },
            function(error){
                console.log("Gyro Stop Error");
            },"XGyroscope","stop",[]);
        }
    }
}